package lessons.lesson_02_syntax.switch_test;

/**
 * Created by penek on 10.07.2016.
 */
public class Switch_03 {

    static final int ONE = 1;
    static final int TWO = 2;
    static final int THREE = 3;

    public static void main(String[] args) {

        final int i = 4;

        switch(i) {
            case ONE:
                System.out.println("Один");
                break;
            case TWO:
                System.out.println("Два");
                break;
            case THREE:
                System.out.println("Три");
                break;
            default:
                System.out.println("default");
                break;
        }
    }
}
