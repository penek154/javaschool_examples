package lessons.lesson_02_syntax.switch_test;

/**
 * Created by penek on 10.07.2016.
 */
public class Switch_02 {
    public static void main(String[] args) {
        int i = 1;
        switch (i) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 9:
                System.out.println("Нечетная цифра");
                break;
            case 2:
            case 4:
            case 6:
            case 8:
                System.out.println("Четная цифра");
                break;
            default:
                System.out.println("default");
                break;
        }
    }
}
