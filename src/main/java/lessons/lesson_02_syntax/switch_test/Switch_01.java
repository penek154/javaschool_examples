package lessons.lesson_02_syntax.switch_test;

/**
 * Created by penek on 10.07.2016.
 */
public class Switch_01 {
    public static void main(String[] args) {
        int i = 1;
        switch(i) {
            case 1:
                System.out.println("Один");
                break;
            case 2:
                System.out.println("Два");
                break;
            case 3:
                System.out.println("Три");
                break;
            default:
                System.out.println("default");
                break;
        }
    }
}
