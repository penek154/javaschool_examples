package lessons.lesson_02_syntax;

/**
 * Created by penek on 10.07.2016.
 */
public class TestFor_2 {
    public static void main(String[] args) {
        int[] arr = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
        for(int k = arr.length - 1; k >= 0; k-=2) {
            System.out.print(arr[k] + " ");
        }
    }
}
