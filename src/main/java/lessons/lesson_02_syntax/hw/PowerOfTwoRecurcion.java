package lessons.lesson_02_syntax.hw;

import java.util.Scanner;

/**
 * Created by penek on 10.07.2016.
 */
public class PowerOfTwoRecurcion {
    public static void main(String[] args) {

        System.out.println("Введите степень 2:");
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) {
            f(1, 1, scanner.nextInt());
            System.out.println("\nВведите степень 2:");
        }
    }

    public static void f(int x, int i, int n) {
        if (i <= n) {
            f(x * 2, i + 1, n);
        }
        System.out.print(x + " ");
    }
}
