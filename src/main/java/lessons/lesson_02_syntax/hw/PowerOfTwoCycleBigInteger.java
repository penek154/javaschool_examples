package lessons.lesson_02_syntax.hw;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by penek on 10.07.2016.
 */
public class PowerOfTwoCycleBigInteger {
    public static void main(String[] args) {

        System.out.println("Введите степень 2:");
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) {
            int n = scanner.nextInt();
            BigInteger x = BigInteger.ONE.shiftLeft(n);
            f(x, n);
            System.out.println();
            System.out.println("Введите степень 2:");
        }

    }

    public static void f(BigInteger x, int n) {
        for (int i = n; i >= 0; i--) {
            System.out.print(x + " ");
            x = x.shiftRight(1);
        }
    }

}
