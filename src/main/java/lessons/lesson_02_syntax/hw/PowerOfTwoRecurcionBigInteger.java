package lessons.lesson_02_syntax.hw;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by penek on 10.07.2016.
 */
public class PowerOfTwoRecurcionBigInteger {
    public static void main(String[] args) {

        System.out.println("Введите степень 2:");
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) {
            f(BigInteger.ONE, 1, scanner.nextInt());
            System.out.println("\nВведите степень 2:");
        }
    }

    public static void f(BigInteger x, int i, int n) {
        if (i <= n) {
            f(x.shiftLeft(1), i + 1, n);
        }
        System.out.print(x + " ");
    }

}
