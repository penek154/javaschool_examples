package lessons.lesson_02_syntax.hw;

import java.util.Scanner;

/**
 * Created by penek on 10.07.2016.
 */
public class PowerOfTwoCycle {
    public static void main(String[] args) {

        System.out.println("Введите степень 2:");
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) {
            f(scanner.nextInt());
            System.out.println();
            System.out.println("Введите степень 2:");
        }

    }

    public static void f(int n) {
        for (int i = n; i >= 0; i--) {
            System.out.print((int) Math.pow(2, i) + " ");
        }
    }

}
