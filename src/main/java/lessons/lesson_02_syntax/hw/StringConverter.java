package lessons.lesson_02_syntax.hw;

import java.util.Scanner;

/**
 * Created by penek on 25.02.2017.
 */
public class StringConverter {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        convert(string);
    }

    static void convert(String str) {
        for (int i = str.length() - 1; i >= 0; i--) {
            System.out.print(str.charAt(i));
        }
    }

}