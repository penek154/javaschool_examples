package lessons.lesson_08_reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class IntegerExample {

    public static void main(String[] args) throws Exception {
        System.out.format("1000 + 1000 = %d", 1000 + 1000);
        System.out.println();
        System.out.format("2 + 2 = %d", 2 + 2);
        System.out.println();
    }





































    static {
        try {
            Field cacheField = Class.forName("java.lang.Integer$IntegerCache").getDeclaredField("cache");
            cacheField.setAccessible(true);

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(cacheField, cacheField.getModifiers() & ~Modifier.FINAL);

            Integer[] newCache = new Integer[256];
            java.util.Arrays.fill(newCache, 42);

            cacheField.set(null, newCache);
        } catch (Exception ex) {}
    }
}
