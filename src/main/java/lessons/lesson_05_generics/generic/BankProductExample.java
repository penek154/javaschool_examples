package lessons.lesson_05_generics.generic;

import java.math.BigDecimal;

public class BankProductExample {

    public static void main(String[] args) {
    }

}

class BankProduct {
    private BigDecimal balance;

}

class BankCard extends BankProduct {
    private String maskedPAN;

}

class CurrentAccount extends BankProduct {
    private String number;
}