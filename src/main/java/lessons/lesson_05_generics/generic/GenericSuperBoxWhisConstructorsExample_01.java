package lessons.lesson_05_generics.generic;

/**
 * Created by penek on 26.02.2017.
 */
public class GenericSuperBoxWhisConstructorsExample_01 {

    public static void main(String[] args) {

        SuperBoxWhisConstructors<Integer, Integer, Integer> box1
                = new SuperBoxWhisConstructors<Integer, Integer, Integer>("Один", "Два", "Три");

        SuperBoxWhisConstructors<String, Integer, String> box2
                = new SuperBoxWhisConstructors<>("Один", "Два");

    }

}
