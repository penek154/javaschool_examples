package lessons.lesson_05_generics.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penek on 28.02.2017.
 */
public class Wildcards_02 {

    public static void main(String[] args) {

        List<?> intList = new ArrayList<Integer>();

        System.out.println(intList.size());

        //intList.add(new Integer(5));

    }
}
