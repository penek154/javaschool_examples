package lessons.lesson_05_generics.generic;

/**
 * Created by penek on 26.02.2017.
 */
public class GenericBoxExample_03 {

    public static void main(String[] args) {

        Box box = new Box();

        box.set(new Integer(5));
        System.out.println(box.get());

        box.set(new String("5"));
        System.out.println(box.get());

    }

}