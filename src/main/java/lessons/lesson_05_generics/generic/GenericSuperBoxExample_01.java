package lessons.lesson_05_generics.generic;

/**
 * Created by penek on 26.02.2017.
 */
public class GenericSuperBoxExample_01 {

    public static void main(String[] args) {

        SuperBox<String,String,String> superBox1 = new SuperBox<>();

        SuperBox<String,Integer,String> superBox2 = new SuperBox<>();

        SuperBox<String,Object,SuperBox> superBox3 = new SuperBox<>();

    }

}
