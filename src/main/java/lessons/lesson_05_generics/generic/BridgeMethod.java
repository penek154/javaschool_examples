package lessons.lesson_05_generics.generic;

import java.lang.reflect.Method;

public class BridgeMethod {

    public static void main(String[] args) {
        for (Method method : Holder.class.getDeclaredMethods()) {
            System.out.println(method.toString() + " " + method.isBridge());
        }
    }

}

class Holder implements Comparable<Holder> {
    @Override
    public int compareTo(Holder o) {
        return 0;
    }
}
