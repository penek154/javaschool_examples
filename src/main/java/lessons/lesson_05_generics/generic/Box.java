package lessons.lesson_05_generics.generic;

/**
 * Created by penek on 26.02.2017.
 */

/**
*    class name<T1, T2, ..., Tn>
*    T1, T2, ..., Tn - это типы пираметров, которые могут быть использованы внутри класса.
*    Допустимые заначения для T1, T2, ..., Tn: class type, interface type, any array type.
*    Типы параметров не могут быть примитивными типами.
**/

public class Box<T> {
    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }
}
