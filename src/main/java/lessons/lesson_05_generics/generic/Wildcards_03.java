package lessons.lesson_05_generics.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penek on 28.02.2017.
 */
public class Wildcards_03 {

    public static void main(String[] args) {

        List<? super Integer> intList = new ArrayList<Integer>();

        intList.add(new Integer(10));

    }
}
