package lessons.lesson_05_generics.generic;

/**
 * Created by penek on 26.02.2017.
 */
public class GenericBoxExample_01 {

    public static void main(String[] args) {

        Box<Integer> integerBox = new Box<Integer>();
        integerBox.set(new Integer(5));
        System.out.println(integerBox.get() + 10);

        Box<String> stringBox = new Box<String>();
        stringBox.set(new String("Привет"));
        System.out.println(stringBox.get());

    }

}
