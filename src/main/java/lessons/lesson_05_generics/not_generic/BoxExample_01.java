package lessons.lesson_05_generics.not_generic;

/**
 * Created by penek on 26.02.2017.
 */
public class BoxExample_01 {

    public static void main(String[] args) {

        Box box = new Box();

        box.set(new String("Привет"));
        System.out.println(box.get());

        box.set(new Integer(1));
        System.out.println(box.get());

    }

}
