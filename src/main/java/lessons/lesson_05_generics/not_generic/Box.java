package lessons.lesson_05_generics.not_generic;

/**
 * Created by penek on 26.02.2017.
 */
public class Box {
    private Object object;

    public void set(Object object) {
        this.object = object;
    }

    public Object get() {
        return object;
    }
}