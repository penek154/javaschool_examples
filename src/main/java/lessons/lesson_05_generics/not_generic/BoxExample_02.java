package lessons.lesson_05_generics.not_generic;

/**
 * Created by penek on 26.02.2017.
 */
public class BoxExample_02 {

    public static void main(String[] args) {

        Box box = new Box();

        box.set(new Integer(5));
        System.out.println((Integer) box.get() + 10);

        //box.set(new String("5"));
        //System.out.println((Integer) box.get() + 10);

    }

}
