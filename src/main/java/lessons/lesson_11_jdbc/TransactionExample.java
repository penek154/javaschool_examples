package lessons.lesson_11_jdbc;

import java.sql.*;

public class TransactionExample {

    public static void main(String[] args) throws Exception {
        createTestDB();

        try (Connection connection = createConnection();
             PreparedStatement updateStatement = connection.prepareStatement("UPDATE account SET balance = balance + ? WHERE number = ?")) {
            connection.setAutoCommit(false);

            updateStatement.setInt(1, 1000000);
            updateStatement.setString(2, "1");
            updateStatement.execute();
            updateStatement.setInt(1, 1000000);
            updateStatement.setString(2, "2");
            updateStatement.execute();
            connection.rollback();

            updateStatement.setInt(1, -1000);
            updateStatement.setString(2, "1");
            updateStatement.execute();
            updateStatement.setInt(1, 1000);
            updateStatement.setString(2, "2");
            updateStatement.execute();
            connection.commit();

            updateStatement.setInt(1, 1000);
            updateStatement.setString(2, "1");
            updateStatement.execute();
            updateStatement.setInt(1, Integer.MAX_VALUE);
            updateStatement.setString(2, "2");
            updateStatement.execute();
            connection.commit();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM account")) {
            while (resultSet.next()) {
                System.out.println(resultSet.getString("number") + " - " + resultSet.getInt("balance"));
            }
        }
    }

    private static void createTestDB() throws Exception {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.addBatch("CREATE TABLE account (number VARCHAR, balance INTEGER, PRIMARY KEY (number));");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('1', 1000);");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('2', 5000);");
            statement.executeBatch();
        }
    }

    private static Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:example;DB_CLOSE_DELAY=-1");
    }
}
