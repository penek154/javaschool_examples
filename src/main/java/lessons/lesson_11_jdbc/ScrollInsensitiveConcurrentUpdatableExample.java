package lessons.lesson_11_jdbc;

import java.sql.*;

/**
 * Пример работы с ResultSet в режиме ResultSet.TYPE_SCROLL_INSENSITIVE
 * и ResultSet.CONCUR_UPDATABLE.
 */
public class ScrollInsensitiveConcurrentUpdatableExample {

    public static void main(String[] args) throws Exception {
        System.out.println("ResultSet.TYPE_SCROLL_INSENSITIVE = "
                + getConnection().getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE));
        System.out.println("ResultSet.CONCUR_UPDATABLE = "
                + getConnection().getMetaData().supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE));
        createTestDB();

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                     ResultSet.CONCUR_UPDATABLE)) {
            try (ResultSet resultSet = statement.executeQuery("SELECT * FROM account;")) {
                System.out.println("Остатки по счетам до начисления процентов:");
                while (resultSet.next()) {
                    System.out.println(resultSet.getString("number") + " - " + resultSet.getInt("balance"));
                }

                // начисление процентов
                while (resultSet.previous()) {
                    int balance = resultSet.getInt("balance");
                    resultSet.updateInt("balance", (int) (balance * 1.05));
                    resultSet.updateRow();
                }

                System.out.println("Остатки по счетам после начисления процентов:");
                while (resultSet.next()) {
                    System.out.println(resultSet.getString("number") + " - " + resultSet.getInt("balance"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void createTestDB() throws Exception {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            statement.addBatch("CREATE TABLE account (number VARCHAR, balance INTEGER, PRIMARY KEY (number));");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('1', 1000);");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('2', 5000);");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('3', 10000);");
            statement.executeBatch();
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:example;DB_CLOSE_DELAY=-1");
    }
}
