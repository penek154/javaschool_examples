package lessons.lesson_11_jdbc;

import java.sql.*;
import java.util.concurrent.TimeUnit;

/**
 * Пример уровней изолированности транзакций read uncommited и read commited.
 */
public class TransactionIsolationExample {

    public static void main(String[] args) throws Exception {
        doWithIsolationLevel(Connection.TRANSACTION_READ_UNCOMMITTED);
        doWithIsolationLevel(Connection.TRANSACTION_READ_COMMITTED);
    }

    private static void doWithIsolationLevel(final int isolationLevel) throws Exception {
        initTestDB();

        Thread readThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(2);

                    try (Connection connection = createConnection()) {
                        connection.setTransactionIsolation(isolationLevel);
                        connection.setAutoCommit(false);
                        try (Statement statement = connection.createStatement();
                             ResultSet resultSet = statement.executeQuery("SELECT * FROM account")) {
                            while (resultSet.next()) {
                                System.out.println(resultSet.getString("number") + " - " + resultSet.getInt("balance"));
                            }
                        }
                        connection.commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Thread writeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    try (Connection connection = createConnection()) {
                        connection.setTransactionIsolation(isolationLevel);
                        connection.setAutoCommit(false);
                        try (Statement statement = connection.createStatement()) {
                            statement.executeUpdate("UPDATE account SET balance = 1000 * balance");
                        }

                        TimeUnit.SECONDS.sleep(4);
                        connection.rollback();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        readThread.start();
        writeThread.start();

        readThread.join();
        writeThread.join();
    }

    private static void initTestDB() throws Exception {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.addBatch("DROP TABLE IF EXISTS account;");
            statement.addBatch("CREATE TABLE account (number VARCHAR, balance INTEGER, PRIMARY KEY (number));");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('1', 1000);");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('2', 5000);");
            statement.executeBatch();
        }
    }

    private static Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:example;DB_CLOSE_DELAY=-1;MVCC=FALSE;LOCK_TIMEOUT=10000");
    }

}
