package lessons.lesson_11_jdbc;

import com.atomikos.icatch.jta.UserTransactionManager;
import com.atomikos.jdbc.AtomikosDataSourceBean;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

/**
 * Пример использования распределённых транзакций.
 */
public class DistributedTransactionExample {

    public static void main(String[] args) throws Exception {
        DataSource dataSource1 = createH2DataSource();
        DataSource dataSource2 = createDerbyDataSource();

        // инициализация баз данных (в одной - информация о счетах, во второй - информация о исполненных переводах)
        try (Connection connection = dataSource1.getConnection();
             Statement statement = connection.createStatement()) {
            statement.addBatch("CREATE TABLE account (number VARCHAR, balance INTEGER, PRIMARY KEY (number));");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('1', 1000);");
            statement.addBatch("INSERT INTO account (number, balance) VALUES ('2', 5000);");
            statement.executeBatch();
        }

        try (Connection connection = dataSource2.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE transfer (ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                    + "source_account VARCHAR(20), destination_account VARCHAR(20), transfer_sum INTEGER, CONSTRAINT primary_key PRIMARY KEY (ID))");
        }

        UserTransactionManager transactionManager = new UserTransactionManager();
        try {
            transactionManager.init();

            try {
                // без ошибки
                transactionManager.begin();
                try (Connection connection = dataSource1.getConnection();
                     PreparedStatement updateStatement = connection.prepareStatement("UPDATE account SET balance = balance + ? WHERE number = ?")) {

                    updateStatement.setInt(1, -500);
                    updateStatement.setString(2, "1");
                    updateStatement.execute();
                    updateStatement.setInt(1, 500);
                    updateStatement.setString(2, "2");
                    updateStatement.execute();
                }

                try (Connection connection = dataSource2.getConnection();
                     PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO transfer (source_account, destination_account, transfer_sum) VALUES (?, ?, ?)")) {

                    insertStatement.setString(1, "1");
                    insertStatement.setString(2, "2");
                    insertStatement.setInt(3, 500);
                    insertStatement.execute();
                }

                transactionManager.commit();
            } catch (Exception ex) {
                transactionManager.rollback();
            }

            try {
                // с ошибкой
                transactionManager.begin();
                try (Connection connection = dataSource1.getConnection();
                     PreparedStatement updateStatement = connection.prepareStatement("UPDATE account SET balance = balance + ? WHERE number = ?")) {

                    updateStatement.setInt(1, -5000);
                    updateStatement.setString(2, "1");
                    updateStatement.execute();
                    updateStatement.setInt(1, 5000);
                    updateStatement.setString(2, "2");
                    updateStatement.execute();
                }

                try (Connection connection = dataSource2.getConnection();
                     PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO transfer (source_account, destination_account, transfer_sum) VALUES (?, ?, ?)")) {

                    insertStatement.setString(1, "1");
                    insertStatement.setString(2, "2");
                    //insertStatement.setInt(3, 5000);
                    insertStatement.execute();
                }

                transactionManager.commit();
            } catch (Exception ex) {
                transactionManager.rollback();
            }

        } finally {
            transactionManager.close();
        }

        try (Connection connection = dataSource1.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM account ORDER BY number ASC")) {
            while (resultSet.next()) {
                System.out.println(resultSet.getString("number") + " - " + resultSet.getInt("balance"));
            }
        }

        try (Connection connection = dataSource2.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM transfer ORDER BY id ASC")) {
            while (resultSet.next()) {
                System.out.println("Transfer " + resultSet.getInt("transfer_sum")
                        + " from " + resultSet.getString("source_account")
                        + " to " + resultSet.getString("destination_account"));
            }
        }
    }

    private static DataSource createH2DataSource() {
        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
        ds.setUniqueResourceName("h2-xs-resource");
        ds.setXaDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
        Properties p = new Properties();
        p.setProperty("user", "test");
        p.setProperty("password", "test");
        p.setProperty("url", "jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1");
        ds.setXaProperties(p);
        ds.setPoolSize(3);
        return ds;
    }

    private static DataSource createDerbyDataSource() {
        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
        ds.setUniqueResourceName("derby-xs-resource");
        ds.setXaDataSourceClassName("org.apache.derby.jdbc.EmbeddedXADataSource");
        Properties p = new Properties();
        p.setProperty("user", "test");
        p.setProperty("password", "test");
        p.setProperty("databaseName", "memory:db2;create=true");
        ds.setXaProperties(p);
        ds.setPoolSize(3);
        return ds;
    }
}
