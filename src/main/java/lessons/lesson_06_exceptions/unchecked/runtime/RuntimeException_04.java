package lessons.lesson_06_exceptions.unchecked.runtime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by penek on 03.03.2017.
 */
public class RuntimeException_04 {

    public static void main(String[] args) {
        try {
            Executor.f();
        } catch (ClassCastException e) {
            System.out.println("Сообщение об ошибке: " + e.getMessage());
        }
    }

    static class Executor {

        static void f() throws ClassCastException {

            List list = new ArrayList();
            list.add(1);
            list.add("два");

            Iterator iter = list.iterator();

            while (iter.hasNext()) {
                int x = (int) iter.next();
                System.out.println(x);
            }
        }
    }
}


