package lessons.lesson_06_exceptions.unchecked.runtime;

/**
 * Created by penek on 03.03.2017.
 */
public class RuntimeException_01 {

    public static void main(String[] args) {
        f();
    }

    static void f() {
        int[] a = new int[10];
        for (int i = 0; i <= 10; i++) {
            a[i] = i;
        }
    }

}
