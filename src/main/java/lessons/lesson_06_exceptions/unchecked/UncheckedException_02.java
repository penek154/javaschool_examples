package lessons.lesson_06_exceptions.unchecked;

/**
 * Created by penek on 01.03.2017.
 */
public class UncheckedException_02 {

    public static void main(String[] args) {

        f();

    }

    static void f() throws RuntimeException {

    }

}
