package lessons.lesson_06_exceptions.unchecked;

/**
 * Created by penek on 01.03.2017.
 */
public class UncheckedException_03 {

    public static void main(String[] args) {

        MyException e = new MyException("Очень страшное исключение");

        throw e;
    }


    static class MyException extends RuntimeException {

        MyException(String mgs) {
            super(mgs);
        }

    }

}
