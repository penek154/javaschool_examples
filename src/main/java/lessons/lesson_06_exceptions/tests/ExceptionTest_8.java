package lessons.lesson_06_exceptions.tests;

class ExceptionTest_8 {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println(1);
            throw new Exception();
        } catch (Exception e) {
            System.out.println(2);
            try {
                System.out.println(3);
                throw new Error();
            } catch (Exception ex) {
                System.out.println(4);
            } finally {
                System.out.println(5);
            }
        } finally {
            System.out.println(6);
        }
    }
}
