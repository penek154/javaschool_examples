package lessons.lesson_06_exceptions.tests;

import java.util.ArrayList;

class ExceptionTest_9 {
    public static void main(String[] args) throws Throwable {

        ArrayList<Throwable> th = new ArrayList<>();
        th.add(new RuntimeException());
        th.add(new Error());
        th.add(new Exception());

        for (Throwable t : th) {
            try {
                System.out.println("1");
                throw t;
            } catch (RuntimeException re) {
                System.out.println("2");
            } catch (Error er) {
                System.out.println("3");
            } catch (Exception ex) {
                System.out.println("4");
            }
        }
    }
}
