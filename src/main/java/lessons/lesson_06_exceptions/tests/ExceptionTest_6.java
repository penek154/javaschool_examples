package lessons.lesson_06_exceptions.tests;

import java.io.IOException;

/**
 * Created by penek on 10.07.2016.
 */
class ExceptionTest_6 {
    public static void main(String[] args) throws Exception {
        f();
        System.out.println("1");
    }

    static void f() throws IOException {
        try {
            throw new IOException();
        } catch (RuntimeException e) {
            System.out.println("2");
        } finally {
            new RuntimeException();
        }
    }
}