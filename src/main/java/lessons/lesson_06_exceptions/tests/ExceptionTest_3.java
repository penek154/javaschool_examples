package lessons.lesson_06_exceptions.tests;

/**
 * Created by penek on 10.07.2016.
 */
class ExceptionTest_3 {
    public static void main(String[] args) {
        try {
            System.out.println(0);
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println(1);
            throw new ArithmeticException();
        } catch (ArithmeticException e) {
            System.out.println(2);
        }
        System.out.println(5);
    }
}