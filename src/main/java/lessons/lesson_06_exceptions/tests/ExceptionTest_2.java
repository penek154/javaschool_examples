package lessons.lesson_06_exceptions.tests;

/**
 * Created by penek on 10.07.2016.
 */
class ExceptionTest_2 {
    public static void main(String[] args) {
        try {
            System.out.println(0);
            throw new RuntimeException();
        } catch (NullPointerException e) {
            System.out.println(1);
        } catch (RuntimeException e) {
            System.out.println(2);
        } catch (Exception e) {
            System.out.println(3);
        }
        System.out.println(4);
    }
}
