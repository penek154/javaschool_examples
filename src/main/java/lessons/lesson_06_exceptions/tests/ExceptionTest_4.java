package lessons.lesson_06_exceptions.tests;

/**
 * Created by penek on 10.07.2016.
 */
class ExceptionTest_4 {
    public static void main(String[] args) {
        System.out.println(f());
    }
    public static int f() {
        try {
            System.out.println(0);
            return 42;
        } finally {
            System.out.println(1);
            return 24;
        }
    }
}
