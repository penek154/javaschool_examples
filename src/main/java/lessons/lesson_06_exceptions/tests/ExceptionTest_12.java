package lessons.lesson_06_exceptions.tests;

public class ExceptionTest_12 {

    private static void weNeedToGoDeeper() {
        try {
            weNeedToGoDeeper();
        } finally {
            weNeedToGoDeeper();
        }
    }

    public static void main(String[] args) {
        weNeedToGoDeeper();
    }
}
