package lessons.lesson_06_exceptions.tests;

/**
 * Created by penek on 10.07.2016.
 */
class ExceptionTest_1 {
    public static void main(String[] args) {
        try {
            System.out.println(0);
            throw new RuntimeException();
        } catch (RuntimeException e) {
            System.out.println(1);
        }
        System.out.println(2);
    }
}
