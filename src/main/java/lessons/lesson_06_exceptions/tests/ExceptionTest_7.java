package lessons.lesson_06_exceptions.tests;

class ExceptionTest_7 {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println(1);
            throw new Exception();
        } catch (NullPointerException e) {
            System.out.println(2);
            throw new ArithmeticException();
        } catch (ArithmeticException e) {
            System.out.println(3);
        } catch (RuntimeException e) {
            System.out.println(4);
            throw new RuntimeException();
        }
        System.out.println(5);
    }
}