package lessons.lesson_06_exceptions.tests;

class ExceptionTest_11 {
    public static void main(String[] args) throws Throwable {
        System.out.println(f());
    }

    static String f() throws Throwable {
        try {
            System.out.println("1");
            throw new Throwable();
        } catch (Exception e) {
            System.out.println("2");
            return "3";
        } finally {
            try {
                System.out.println("4");
                throw new Exception();
            } catch (Throwable e) {
                System.out.println("5");
                return "6";
            } finally {
                System.out.println("7");
                return "8";
            }
        }
    }
}
