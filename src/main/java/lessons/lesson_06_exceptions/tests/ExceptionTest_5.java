package lessons.lesson_06_exceptions.tests;

/**
 * Created by penek on 10.07.2016.
 */
class ExceptionTest_5 {
    public static void main(String[] args) {
        try {
            System.out.println(0);
            if (true) {
                throw new NullPointerException();
            }
        } catch (RuntimeException e) {
            System.out.println(1);
        } finally {
            System.out.println(5);
        }
        System.out.println(6);
    }
}
