package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class RefValueQuiz_06_String_String {
    public static void main(String[] args) {
        String x = new String("0");
        changeNumber(x);
        System.out.println(x);
    }

    static void changeNumber(String x) {
        x = "1";
    }

}
