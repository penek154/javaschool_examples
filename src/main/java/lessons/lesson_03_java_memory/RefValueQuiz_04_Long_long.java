package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class RefValueQuiz_04_Long_long {
    public static void main(String[] args) {
        Long x = new Long(0);
        changeNumber(x);
        System.out.println(x);
    }

    static void changeNumber(long x) {
        x = 1;
    }

}
