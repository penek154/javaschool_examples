package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class RefValueQuiz_05_Long_Long {
    public static void main(String[] args) {
        Long x = new Long(0);
        changeNumber(x);
        System.out.println(x);
    }

    static void changeNumber(Long x) {
        x = 1L;
    }

}
