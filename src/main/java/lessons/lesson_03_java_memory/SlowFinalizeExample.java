package lessons.lesson_03_java_memory;

/**
 * Демонстрация того, как сборщик мусора не успевает удалять объекты из памяти.
 *
 * Согласно спецификации, метод finalize вызывается после того, как JVM определила,
 * что объект может быть удалён из памяти. Все такие объекты помещаются в специальную
 * очередь, которую обрабатывает специальный поток JVM, вызывая для каждого такого
 * объекта метод finalize.
 * Если метод finalize будет отрабатывать медленно, то очередь может переполнится, тем
 * самым спровоцируется ошибка java.lang.OutOfMemoryError: Java heap space.
 *
 * Для сбора статистики работы сборщика мусора нужно запустить JVM с параметрами
 * -XX:+PrintGCDateStamps -verbose:gc -XX:+PrintGCDetails -Xloggc:"<path to log>"
 *
 * Для снятия heap dump при падении JVM при нехватке памяти нужно запустить JVM с параметрами
 * -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=<path to dump>.hprof
 *
 * Для демонстрации примера следует использовать такие настройки запуска JVM:
 * -Xmx200M -XX:+PrintGCDateStamps -verbose:gc -XX:+PrintGCDetails -Xloggc:"gc-log.log" -XX:+HeapDumpOnOutOfMemoryError
 */
public class SlowFinalizeExample {

    public static void main(String[] args) {
        while (true) {
            new ClassWithFinalize();
        }
    }
}

class ClassWithFinalize {

    private int val[];

    ClassWithFinalize() {
        val = new int[1000];
    }

    @Override
    protected void finalize() throws Throwable {
        // заставим заснуть поток на 0,1 секунду
        Thread.sleep(100);
    }
}
