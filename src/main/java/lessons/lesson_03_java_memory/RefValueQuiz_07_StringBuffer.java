package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class RefValueQuiz_07_StringBuffer {

    public static void main(String[] args) {

        StringBuffer x = new StringBuffer("0");
        changeNumber(x);
        System.out.println(x);
    }

    static void changeNumber(StringBuffer x) {
        x.replace(0, 1, "1");
    }

}
