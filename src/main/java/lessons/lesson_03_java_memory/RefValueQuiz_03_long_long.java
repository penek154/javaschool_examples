package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class RefValueQuiz_03_long_long {
    public static void main(String[] args) {
        long x = 0;
        changeNumber(x);
        System.out.println(x);
    }

    static void changeNumber(long x) {
        x = 1;
    }

}
