package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class Stack_02 {

    static int i;

    public static void main(String[] args) {
        test(i);
    }

    private static void test(int i) {

        i = i + 1;
        try {
            test(i);
        } catch (StackOverflowError e) {
            System.out.println(i);
        }

    }
}