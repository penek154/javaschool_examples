package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
class Heap_04 {
    public static void main(String[] args) {
        Object[] ref = new Object[1];
        while (true) {
            Object[] a = new Object[1];
            Object[] b = new Object[1];
            a[0] = b;
            b[0] = a;
            ref[0] = a;
        }
    }
}
