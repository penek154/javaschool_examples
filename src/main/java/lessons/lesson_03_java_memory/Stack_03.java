package lessons.lesson_03_java_memory;

/**
 * Created by penek on 10.07.2016.
 */
public class Stack_03 {

    static int i;

    public static void main(String[] args)
    {
        test(i);
    }

    private static void test(int i) {

        Integer i1 = new Integer(1);
        Integer i2 = new Integer(2);
        Integer i3 = new Integer(3);
        Integer i4 = new Integer(4);
        Integer i5 = new Integer(5);

        i = i + 1;
        try {
            test(i);
        } catch (StackOverflowError e) {
            System.out.println(i);
        }

    }
}