package lessons.lesson_04_collections;

import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class TreeSetExample {

    private static final Logger LOG = Logger.getLogger(TreeSetExample.class);

    public static void main(String[] args) {

        LinkedHashSet<User> users = new LinkedHashSet<User>();
        System.out.println("Создание множества пользователей.");
        users.add(new User("Маша", 18));
        users.add(new User("Петя", 15));
        users.add(new User("Даша", 75));
        System.out.println("Вывод на экран множества пользователей в порядке их добавления в множество:");
        System.out.println(users.toString());
        System.out.println();

        TreeSet<User> nameSortUsers = new TreeSet<>(new NameUserSort());
        nameSortUsers.addAll(users);
        System.out.println("Вывод на экран множества пользователей, отсортированных по имени:");
        System.out.println(nameSortUsers.toString());
        System.out.println();

        TreeSet<User> ageSortUsers = new TreeSet<>(new AgeUserSort());
        ageSortUsers.addAll(users);
        System.out.println("Вывод на экран множества пользователей, отсортированных по возрасту:");
        System.out.println(ageSortUsers.toString());
        System.out.println();

    }
}

class User implements Comparable<User> {

    String name;
    int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(User that) {
        return this.name.compareTo(that.name);
    }

    @Override
    public String toString() {
        return "User(Имя: " + this.name + ", возраст: " + this.age + ")";
    }

}

class NameUserSort implements Comparator<User> {

    private static final Logger LOG = Logger.getLogger(NameUserSort.class);

    @Override
    public int compare(User o1, User o2) {
        return o1.name.compareTo(o2.name);
    }

}

class AgeUserSort implements Comparator<User> {

    private static final Logger LOG = Logger.getLogger(AgeUserSort.class);

    @Override
    public int compare(User o1, User o2) {

        Integer age1 = o1.age;
        Integer age2 = o2.age;

        return age1.compareTo(age2);
    }

}