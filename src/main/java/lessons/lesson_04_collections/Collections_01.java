package lessons.lesson_04_collections;

import java.util.*;

public class Collections_01 {

    static final Integer[] arr = {5, 2, 7, 5, 4};

    public static void main(String[] args) {

        List<Integer> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(arr));
        System.out.println("arrayList: " + arrayList.toString());

        List<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(Arrays.asList(arr));
        System.out.println("linkedList: " + linkedList.toString());

        Set<Integer> treeSet = new TreeSet<>();
        treeSet.addAll(Arrays.asList(arr));
        System.out.println("treeSet: " + treeSet.toString());
    }
}
