package lessons.lesson_04_collections.hw;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Решение домашнего задания по теме "Коллекции".
 * Задание такое:
 * Исходные данные: текстовый файл из нескольких строк, содержащий слова, разделённые пробелом.
 * Все буквы в словах в верхнем регистре.
 *
 * Задание 1: Подсчитайте количество различных слов в файле.
 * Задание 2: Выведите на экран список различных слов файла, отсортированный по возрастанию
 * их длины (компаратор сначала по длине слова, потом по тексту).
 * Задание 3: Подсчитайте и выведите на экран сколько раз каждое слово встречается в файле.
 * Задание 4: Реализуйте свой Iterator для обхода списка в обратном порядке. Выведите на экран все строки файла в обратном порядке.
 * Задание 5: Выведите на экран строки, номера которых задаются пользователем в произвольном порядке.
 *
 * В реализациях используйте наиболее подходящие имплементации коллекций.
 */
public class CollectionsHomeWork {

    private static class ReverseListIterator<T> implements Iterator<T> {

        private List<T> collection;
        private int index;

        ReverseListIterator(List<T> collection) {
            this.collection = new ArrayList<>(collection);
            Collections.reverse(collection);
            index = 0;
        }

        @Override
        public boolean hasNext() {
            return index < collection.size();
        }

        @Override
        public T next() {
            return collection.get(index++);
        }

        @Override
        public void remove() {
            collection.remove(index);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("src/main/java/lessons/lesson_04_collections/hw/collections-home-work.txt");
        countWordsInFile(file);
        printWordsInFile(file);
        printWordsCountInFile(file);
        printReverseFile(file);
        int lineNumbers[] = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            lineNumbers[i] = Integer.parseInt(args[i]);
        }
        printSomeLinesFromFile(file, lineNumbers);
    }

    private static void countWordsInFile(File file) throws FileNotFoundException {
        System.out.println("1. Подсчитайте количество различных слов в файле.");
        Scanner scanner = new Scanner(file);
        Set<String> words = new HashSet<>();
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] wordsInLine = line.split(" ");
            Collections.addAll(words, wordsInLine);
        }
        scanner.close();
        System.out.println("Количество слов: " + words.size());
        System.out.println();
    }

    private static void printWordsInFile(File file) throws FileNotFoundException {
        System.out.println("2. Выведите на экран список различных слов файла, отсортированный по возрастанию" +
                " их длины (компаратор сначала по длине слова, потом по тексту).");
        Scanner scanner = new Scanner(file);
        Set<String> words = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.length() != o2.length()) {
                    return o1.length() - o2.length();
                } else {
                    return o1.compareTo(o2);
                }
            }
        });
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] wordsInLine = line.split(" ");
            Collections.addAll(words, wordsInLine);
        }
        scanner.close();
        for (String word : words) {
            System.out.println(word);
        }
        System.out.println();
    }

    private static void printWordsCountInFile(File file) throws FileNotFoundException {
        System.out.println("3. Подсчитайте и выведите на экран сколько раз каждое слово встречается в файле.");
        Scanner scanner = new Scanner(file);
        Map<String, Integer> counts = new HashMap<>();
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] wordsInLine = line.split(" ");
            for (String word : wordsInLine) {
                Integer count = counts.get(word);
                counts.put(word, (count == null) ? 1 : count + 1);
            }
        }
        scanner.close();
        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
        System.out.println();
    }

    private static void printReverseFile(File file) throws FileNotFoundException {
        System.out.println("4. Реализуйте свой Iterator для обхода списка в обратном порядке. " +
                "Выведите на экран все строки файла в обратном порядке.");
        Scanner scanner = new Scanner(file);
        List<String> lines = new ArrayList<>();
        while (scanner.hasNext()) {
            lines.add(scanner.nextLine());
        }
        scanner.close();
        Iterator<String> iterator = new ReverseListIterator<>(lines);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println();
    }

    private static void printSomeLinesFromFile(File file, int... lineNumbers) throws FileNotFoundException {
        System.out.println("5. Выведите на экран строки, номера которых задаются пользователем в произвольном порядке.");
        Scanner scanner = new Scanner(file);
        List<String> lines = new ArrayList<>();
        while (scanner.hasNext()) {
            lines.add(scanner.nextLine());
        }
        scanner.close();
        for (int index : lineNumbers) {
            if ((index >= 1) && (index <= lines.size())) {
                System.out.println(index + " - " + lines.get(index));
            } else {
                System.out.println("Строки с номером " + index + " нет в файле.");
            }
        }
        System.out.println();
    }
}
