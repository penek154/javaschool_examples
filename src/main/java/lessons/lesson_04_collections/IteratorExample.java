package lessons.lesson_04_collections;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by penek on 25.02.2017.
 */
public class IteratorExample {

    public static void main(String[] args) {

        ArrayList<String> carModels = new ArrayList<String>();
        carModels.add("BMW");
        carModels.add("Mercedes");
        carModels.add("Audi");
        carModels.add("Lada");

        Iterator<String> iter = carModels.iterator();
        while(iter.hasNext()){
            System.out.println(iter.next());
        }
    }

}
