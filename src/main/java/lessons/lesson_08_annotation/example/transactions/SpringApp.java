package lessons.lesson_08_annotation.example.transactions;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringApp {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("transactions-context.xml");
        BankSystem bankSystem = context.getBean(BankSystem.class);

        System.out.println("Остаток на счёте 12345: " + bankSystem.getBalance("12345") + " руб.");
        System.out.println("Остаток на счёте 54321: " + bankSystem.getBalance("54321") + " руб.");

        System.out.println("Перевод 1");
        try {
            bankSystem.transfer("12345", "54321", 50);
            System.out.println("Перевод 1 совершён");
        } catch (Exception ex) {
            System.out.println("Произошла ошибка");
        }

        System.out.println("Перевод 2");
        try {
            bankSystem.transfer("54321", "12345", 1000);
            System.out.println("Перевод 2 совершён");
        } catch (Exception ex) {
            System.out.println("Произошла ошибка");
        }

        System.out.println("Остаток на счёте 12345: " + bankSystem.getBalance("12345") + " руб.");
        System.out.println("Остаток на счёте 54321: " + bankSystem.getBalance("54321") + " руб.");
    }
}
