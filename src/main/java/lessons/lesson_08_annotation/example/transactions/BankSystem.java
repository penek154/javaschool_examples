package lessons.lesson_08_annotation.example.transactions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

public class BankSystem {

    @Autowired
    private JdbcTemplate db;

    @PostConstruct
    public void createTable() {
        db.execute("create table account (number VARCHAR, balance int)");
        db.execute("insert into account (number, balance) values ('12345', 1000)");
        db.execute("insert into account (number, balance) values ('54321', 5000)");
    }

    @Transactional
    public void transfer(String sourceAccountNumber, String destinationAccountNumber, int sum) {
        changeBalance(sourceAccountNumber, -sum);
        changeBalance(destinationAccountNumber, sum);
    }

    private void changeBalance(String accountNumber, int value) {
        if (value > 100) {
            throw new RuntimeException("Слишком большая сумма перевода");
        }
        db.execute("update account set balance = balance + (" + value + ") where number = '" + accountNumber + "'");
    }

    public int getBalance(String accountNumber) {
        return db.queryForObject("select balance from account where number = '" + accountNumber + "'", Integer.class);
    }
}
