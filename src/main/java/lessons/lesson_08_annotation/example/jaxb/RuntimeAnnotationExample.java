package lessons.lesson_08_annotation.example.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Пример использования аннотаций с областью видимости RUNTIME.
 */
public class RuntimeAnnotationExample {

    public static void main(String[] args) throws Exception {
        Person person = new Person(1, "Владимир", "Путин");
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account("12345678901234567890"));
        accounts.add(new Account("09876543210987654321"));
        accounts.add(new Account("11223344556677889900"));
        person.setAccounts(accounts);

        JAXBContext context = JAXBContext.newInstance(Person.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(person, System.out);
    }
}

@XmlRootElement(name = "Person")
@XmlAccessorType(XmlAccessType.FIELD)
class Person {

    @XmlAttribute(name = "ID")
    private long id;

    @XmlElement
    private String firstName;

    @XmlElement
    private String secondName;

    @XmlElementWrapper(name = "accounts")
    @XmlElement(name = "account")
    private List<Account> accounts;

    public Person() {
    }

    public Person(long id, String firstName, String secondName) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}

class Account {
    @XmlValue
    private String number;

    public Account() {
    }

    public Account(String number) {
        this.number = number;
    }
}