package lessons.lesson_09_multithreading.example;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicExample {

    static int count;

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newCachedThreadPool();
        final CountDownLatch countDownLatch = new CountDownLatch(1000);
        for (int i = 0; i < 1000; i++) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    count++;
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        System.out.println(count);
        executor.shutdown();




















//        final AtomicInteger count2 = new AtomicInteger();
//        executor = Executors.newCachedThreadPool();
//        final CountDownLatch countDownLatch2 = new CountDownLatch(1000);
//        for (int i = 0; i < 1000; i++) {
//            executor.execute(new Runnable() {
//                @Override
//                public void run() {
//                    count2.getAndIncrement();
//                    countDownLatch2.countDown();
//                }
//            });
//        }
//        countDownLatch2.await();
//        System.out.println(count2.get());
//        executor.shutdown();
    }
}
