package lessons.lesson_09_multithreading.example;

import java.util.concurrent.TimeUnit;

/**
 * Пример прерывания потока.
 */
public class InterruptionExample {

    public static void main(String[] args) throws InterruptedException {
        int count = 0;
        while (count++ <= 100) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {
                        try {
                            TimeUnit.SECONDS.sleep(10);
                        } catch (InterruptedException e) {
                            System.out.println("Я поток " + Thread.currentThread().getName() + ". Меня прервали, когда я был заблокирован");
                            break;
                        }
                    }
                    if (Thread.currentThread().isInterrupted()) {
                        System.out.println("Я поток " + Thread.currentThread().getName() + ". Меня прервали, когда я был активным");
                    }
                }
            }, "Thread-" + count);
            t.start();
            TimeUnit.NANOSECONDS.sleep(1);
            t.interrupt();
            t.join();
        }
    }
}
