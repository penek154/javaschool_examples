package lessons.lesson_09_multithreading.example;

public class WaitNotifyExample {

    public static void main(String[] args) throws InterruptedException {
        final A a = new A();
        for (int i = 1; i <= 5; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        a.hello();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, "Thread-" + i).start();
        }

        Thread.sleep(5000);
        synchronized (a) {
            System.out.println("Notify...");
            a.notify();
            Thread.sleep(3000);
            System.out.println("Notification was send.");
            Thread.sleep(3000);
        }
    }
}

class A {

    private String name;

    public void hello() throws InterruptedException {
        synchronized (this) {
            System.out.println(Thread.currentThread().getName() + " wait...");
            wait(20000);
            System.out.println("Hello! I'm " + Thread.currentThread().getName());
        }
    }
}
