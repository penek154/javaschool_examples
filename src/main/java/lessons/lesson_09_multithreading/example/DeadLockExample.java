package lessons.lesson_09_multithreading.example;

import java.math.BigDecimal;

/**
 * Пример с dead lock'ом.
 */
public class DeadLockExample {

    public static void main(String[] args) throws InterruptedException {
        final Account accountA = new Account("1", new BigDecimal("1000000"));
        final Account accountB = new Account("2", new BigDecimal("2000000"));

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 1000000;
                while (count-- > 0) {
                    TransferExecutor.executeTransfer(accountA, accountB, BigDecimal.ONE);
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 1000000;
                while (count-- > 0) {
                    TransferExecutor.executeTransfer(accountB, accountA, new BigDecimal("2"));
                }
            }
        });

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println("accountA.balance = " + accountA.balance);
        System.out.println("accountB.balance = " + accountB.balance);
    }
}


class TransferExecutor {

    static void executeTransfer(Account source, Account destination, BigDecimal sum) {
        synchronized (source) {
            synchronized (destination) {
                source.balance = source.balance.subtract(sum);
                destination.balance = destination.balance.add(sum);
            }
        }
    }
}

class Account {
    String number;
    BigDecimal balance;

    Account(String number, BigDecimal balance) {
        this.number = number;
        this.balance = balance;
    }
}