package lessons.lesson_09_multithreading.example;

import java.util.concurrent.*;

public class FutureExample {

    private static ExecutorService executorService = Executors.newFixedThreadPool(3);

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Future<Integer> square1 = lazySquare(1);
        Future<Integer> square3 = lazySquare(3);
        Future<Integer> square5 = lazySquare(5);

        System.out.println("3^2 = " + square3.get());
        System.out.println("1^2 = " + square1.get());
        System.out.println("5^2 = " + square5.get());

        executorService.shutdown();
    }

    private static Future<Integer> lazySquare(int value) {
        return executorService.submit(new SquareCalculator(value));
    }
}

class SquareCalculator implements Callable<Integer> {

    private int value;

    public SquareCalculator(int value) {
        this.value = value;
    }

    @Override
    public Integer call() throws Exception {
        TimeUnit.SECONDS.sleep(value * value);
        System.out.println(value + "^2 calculated.");
        return value * value;
    }
}
