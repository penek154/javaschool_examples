package lessons.lesson_09_multithreading.example;

public class VolatileNotAtomicExample {

    static volatile int sum;

    public static void main(String[] args) throws InterruptedException {
        do {
            sum = 100;
            Thread thread1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    sum = sum + 3;
                }
            });

            Thread thread2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    sum = sum + 5;
                }
            });

            thread1.start();
            thread2.start();

            thread1.join();
            thread2.join();

        } while (sum != 105);

    }
}
