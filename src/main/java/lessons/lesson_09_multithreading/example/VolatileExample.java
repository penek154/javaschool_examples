package lessons.lesson_09_multithreading.example;

public class VolatileExample {

    public static void main(String[] args) {
        final Worker worker = new Worker();
        new Thread(worker).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                worker.stop();
            }
        }).start();
    }
}

class Worker implements Runnable {

    private boolean nadoelo = false;

    void stop() {
        nadoelo = true;
    }

    @Override
    public void run() {
        while (!nadoelo) {
        }
        System.out.println("Nadoelo!");
    }
}
